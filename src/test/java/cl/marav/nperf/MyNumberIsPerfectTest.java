package cl.marav.nperf;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyNumberIsPerfectTest {
	MyNumberIsPerfect npe ;
	String result;
	@Before
	public void setup(){
		npe = new MyNumberIsPerfect();
	}
		
	@Test
	public void numberSixReturnYes() {
		result= npe.testingMyNumber(6);
		assertEquals("Yes",result);
		//fail("Not yet implemented");
	}
	
	@Test
	public void numberEightReturnNo(){
		result= npe.testingMyNumber(8);
		assertEquals("No",result);
	}
	
	@Test
	public void number28ReturnYes(){
		result= npe.testingMyNumber(28);
		assertEquals("Yes",result);
		
	}
	@Test
	public void number496ReturnYes(){
		result= npe.testingMyNumber(496);
		assertEquals("Yes",result);
		
	}
	@Test
	public void number8128ReturnYes(){
		result= npe.testingMyNumber(8128);
		assertEquals("Yes",result);
		
	}
	
	@Test
	public void number312ReturnNo(){
		result= npe.testingMyNumber(312);
		assertEquals("No",result);
	}
	
}
