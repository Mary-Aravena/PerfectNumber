package cl.marav.nperf;
public class MyNumberIsPerfect {
	
	public String testingMyNumber(int test){
		int div=0;
		for(int i=1; i<test; i++)
			if(test%i==0)
				div=div+i;
		if(div==test) return "Yes";
		return "No";
	}
}
